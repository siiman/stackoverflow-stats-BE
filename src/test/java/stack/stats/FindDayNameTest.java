package stack.stats;

import org.junit.Test;
import stack.stats.model.input.Question;
import stack.stats.util.DateConverter;

import static org.junit.Assert.assertEquals;

public class FindDayNameTest {
    @Test
    public void testFindSunday() {
        Question question = Question.builder().created(1564931550L).build();
        String test = DateConverter.findDayName(question);
        assertEquals("Sunday", test);
    }
}
