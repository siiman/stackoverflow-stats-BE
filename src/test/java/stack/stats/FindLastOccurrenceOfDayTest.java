package stack.stats;

import org.junit.Test;
import stack.stats.util.DateConverter;

import java.time.LocalDate;
import java.time.LocalTime;

import static org.junit.Assert.assertEquals;

public class FindLastOccurrenceOfDayTest {
    @Test
    public void testFindPreviousWednesdayStartEpochTimeFrom0307() {
        long test = DateConverter.findLastOccurrenceOfDay(LocalDate.of(2019, 7, 3)
                                                                    .atStartOfDay());
        assertEquals(1561507200L, test);
    }

    @Test
    public void testFindPreviousWednesdayEndEpochTimeFrom0307() {
        long test = DateConverter.findLastOccurrenceOfDay(LocalDate.of(2019, 7, 3)
                                                                  .atTime(LocalTime.MAX));
        assertEquals(1561593599L, test);
    }
}
