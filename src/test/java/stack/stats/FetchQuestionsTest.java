package stack.stats;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;
import stack.stats.model.input.StackExchangeWrapper;
import stack.stats.model.input.Question;
import stack.stats.service.StatisticsService;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class FetchQuestionsTest {
    @Mock
    private RestTemplate mockRestTemplate;

    @InjectMocks
    private StatisticsService statisticsService = new StatisticsService();

    @Test
    public void testReturnCorrectObjectWithMockito() {
        List<Question> mockQuestions = new ArrayList<>();
        mockQuestions.add(Question.builder()
                                  .answers(3)
                                  .created(1562440554)
                                  .views(3)
                                  .build());
        mockQuestions.add(Question.builder()
                                  .answers(2)
                                  .created(1562440910)
                                  .views(10)
                                  .build());
        StackExchangeWrapper correct = new StackExchangeWrapper(mockQuestions, false);
        Mockito.when(mockRestTemplate.getForObject(Mockito.anyString(), Mockito.any(Class.class), Mockito.any(Object.class)))
               .thenReturn(correct);

        List<Question> testItems = statisticsService.fetchQuestions("java", 1);
        assertEquals(correct.getItems(), testItems);
    }
}
