package stack.stats;

import org.junit.Test;
import stack.stats.model.input.Question;
import stack.stats.service.StatisticsService;
import stack.stats.util.DateConverter;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CountByClassifiersTest {
    private StatisticsService stats = new StatisticsService();

    @Test
    public void testWithEmptyList() {
        List<Question> questions = new ArrayList<>();
        Map<Integer, Map<String, Long>> test = stats.countByClassifiers(questions, DateConverter::findHourOfDay,
                                                                        DateConverter::findDayName);
        assertTrue(test.isEmpty());
    }

    @Test
    public void testCountWithOneHourOneDay() {
        List<Question> questions = new ArrayList<>();
        questions.add(Question.builder()
                              .created(1562440554L)
                              .build());
        Map<Integer, Map<String, Long>> test = stats.countByClassifiers(questions, DateConverter::findHourOfDay,
                                                                        DateConverter::findDayName);
        Map<Integer, Map<String, Long>> correct = new HashMap<>();
        correct.put(19, Collections.singletonMap("Saturday", 1L));
        assertEquals(correct, test);
    }

    @Test
    public void testCountWithDifferentHoursOnDifferentDays() {
        List<Question> questions = new ArrayList<>();
        questions.add(Question.builder()
                              .created(1562440554L)
                              .build());
        questions.add(Question.builder()
                              .created(1562755377L)
                              .build());
        Map<Integer, Map<String, Long>> test = stats.countByClassifiers(questions, DateConverter::findHourOfDay,
                                                                        DateConverter::findDayName);
        Map<Integer, Map<String, Long>> correct = new HashMap<>();
        correct.put(10, Collections.singletonMap("Wednesday", 1L));
        correct.put(19, Collections.singletonMap("Saturday", 1L));
        assertEquals(correct, test);
    }

    @Test
    public void testCountWithSameDayDifferentHours() {
        List<Question> questions = new ArrayList<>();
        questions.add(Question.builder()
                              .created(1562755759L)
                              .build());
        questions.add(Question.builder()
                              .created(1562751836L)
                              .build());
        Map<Integer, Map<String, Long>> test = stats.countByClassifiers(questions, DateConverter::findHourOfDay,
                                                                        DateConverter::findDayName);
        Map<Integer, Map<String, Long>> correct = new HashMap<>();
        correct.put(10, Collections.singletonMap("Wednesday", 1L));
        correct.put(9, Collections.singletonMap("Wednesday", 1L));
        assertEquals(correct, test);
    }

    @Test
    public void testCountWithSameHourDifferentDays() {
        List<Question> questions = new ArrayList<>();
        questions.add(Question.builder()
                              .created(1562755759L)
                              .build());
        questions.add(Question.builder()
                              .created(1563446827L)
                              .build());
        questions.add(Question.builder()
                              .created(1563445807L)
                              .build());
        Map<Integer, Map<String, Long>> test = stats.countByClassifiers(questions, DateConverter::findHourOfDay,
                                                                        DateConverter::findDayName);
        Map<Integer, Map<String, Long>> correct = new HashMap<>();
        correct.put(10, Stream.of(new AbstractMap.SimpleEntry<>("Wednesday", 1L),
                                  new AbstractMap.SimpleEntry<>("Thursday", 2L))
                              .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));
        assertEquals(correct, test);
    }

    @Test
    public void testCountWithSameHourInADayAndSomeOtherDayWithOtherHour() {
        List<Question> questions = new ArrayList<>();
        questions.add(Question.builder()
                              .created(1562755759L)
                              .build());
        questions.add(Question.builder()
                              .created(1562753719L)
                              .build());
        questions.add(Question.builder()
                              .created(1560190519L)
                              .build());
        Map<Integer, Map<String, Long>> test = stats.countByClassifiers(questions, DateConverter::findHourOfDay,
                                                                        DateConverter::findDayName);
        Map<Integer, Map<String, Long>> correct = new HashMap<>();
        correct.put(10, Collections.singletonMap("Wednesday", 2L));
        correct.put(18, Collections.singletonMap("Monday", 1L));
        assertEquals(correct, test);
    }

    @Test
    public void testCountAnsweredAndNotAnsweredOfWednesdayMonday() {
        List<Question> questions = new ArrayList<>();
        questions.add(Question.builder()
                              .created(1562755759L)
                              .answers(2)
                              .build());
        questions.add(Question.builder()
                              .created(1562753719L)
                              .answers(0)
                              .build());
        questions.add(Question.builder()
                              .created(1560190519L)
                              .answers(3)
                              .build());
        questions.add(Question.builder()
                              .created(1560188993L)
                              .answers(0)
                              .build());
        questions.add(Question.builder()
                              .created(1560199793)
                              .answers(1)
                              .build());
        Map<String, Map<String, Long>> test = stats.countByClassifiers(questions, DateConverter::findDayName,
                                                                       Question::hasAnswers);
        Map<String, Map<String, Long>> correct = new HashMap<>();
        correct.put("Monday", Stream.of(new AbstractMap.SimpleEntry<>("answered", 2L),
                                        new AbstractMap.SimpleEntry<>("not answered", 1L))
                                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));
        correct.put("Wednesday", Stream.of(new AbstractMap.SimpleEntry<>("answered", 1L),
                                           new AbstractMap.SimpleEntry<>("not answered", 1L))
                                       .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));
        assertEquals(correct, test);
    }
}
