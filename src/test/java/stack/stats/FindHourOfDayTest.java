package stack.stats;

import org.junit.Test;
import stack.stats.model.input.Question;
import stack.stats.util.DateConverter;

import static org.junit.Assert.assertEquals;

public class FindHourOfDayTest {
    @Test
    public void testFindHourIs23() {
        Question question = Question.builder().created(1564960350L).build();
        int test = DateConverter.findHourOfDay(question);
        assertEquals(23, test);
    }
}
