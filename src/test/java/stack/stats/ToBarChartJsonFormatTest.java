package stack.stats;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import stack.stats.model.output.Indicator;
import stack.stats.service.StatisticsService;

import java.util.*;

import static org.junit.Assert.assertEquals;

public class ToBarChartJsonFormatTest {
    StatisticsService stats = new StatisticsService();
    ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void testMapWithOneHourAndTwoDays() throws JsonProcessingException {
        Map<Integer, Map<String, Long>> outerMap = new HashMap<>();
        Map<String, Long> innerMap = new HashMap<>();
        innerMap.put("Sunday", 3L);
        innerMap.put("Saturday", 4L);
        outerMap.put(0, innerMap);
        List<Indicator> testList = stats.toChartJsonFormat(outerMap);
        String correct = "[{\"name\":0,\"series\":[{\"name\":\"Sunday\",\"value\":3},{\"name\":\"Saturday\",\"value\":4}]}]";
        String test = objectMapper.writeValueAsString(testList);
        assertEquals(correct, test);
    }

    @Test
    public void testMapWithTwoHoursAndTwoDays() throws JsonProcessingException {
        Map<Integer, Map<String, Long>> outerMap = new HashMap<>();
        Map<String, Long> innerMap = new HashMap<>();
        innerMap.put("Sunday", 3L);
        innerMap.put("Saturday", 4L);
        outerMap.put(0, innerMap);
        outerMap.put(1, innerMap);
        List<Indicator> testList = stats.toChartJsonFormat(outerMap);
        String correct = "[{\"name\":0,\"series\":[{\"name\":\"Sunday\",\"value\":3},{\"name\":\"Saturday\",\"value\":4}]},{\"name\":1,\"series\":[{\"name\":\"Sunday\",\"value\":3},{\"name\":\"Saturday\",\"value\":4}]}]";
        String test = objectMapper.writeValueAsString(testList);
        assertEquals(correct, test);
    }

    @Test
    public void testMapWithSaturdayAndAnsweredNotAnswered() throws JsonProcessingException {
        Map<String, Map<String, Long>> outerMap = new HashMap<>();
        Map<String, Long> innerMap = new HashMap<>();
        innerMap.put("answered", 100L);
        innerMap.put("not answered", 120L);
        outerMap.put("Saturday", innerMap);
        outerMap.put("Sunday", innerMap);
        List<Indicator> testList = stats.toChartJsonFormat(outerMap);
        String correct = "[{\"name\":\"Sunday\",\"series\":[{\"name\":\"answered\",\"value\":100},{\"name\":\"not answered\",\"value\":120}]},{\"name\":\"Saturday\",\"series\":[{\"name\":\"answered\",\"value\":100},{\"name\":\"not answered\",\"value\":120}]}]";
        String test = objectMapper.writeValueAsString(testList);
        assertEquals(correct, test);
    }
}
