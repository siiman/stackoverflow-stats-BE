package stack.stats;

import org.junit.Test;
import stack.stats.model.input.Question;
import stack.stats.service.StatisticsService;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.Assert.assertThat;

public class FindTopThreeTest {
    StatisticsService statisticsService = new StatisticsService();
    List<Question> input = Arrays.asList(Question.builder()
                                                 .views(5)
                                                 .title("five")
                                                 .build(),
                                         Question.builder()
                                                 .views(15)
                                                 .title("fifteen")
                                                 .build(),
                                         Question.builder()
                                                 .views(20)
                                                 .title("twenty")
                                                 .build(),
                                         Question.builder()
                                                 .views(25)
                                                 .title("twenty five")
                                                 .build());
    List<Question> test = statisticsService.findTopThree(input, Question::getViews);

    @Test
    public void testSizeIsThree() {
        assertThat(test, hasSize(3));
    }

    @Test
    public void firstItemIs25() {
        assertThat(test.get(0), samePropertyValuesAs(Question.builder()
                                                             .title("twenty five")
                                                             .views(25)
                                                             .build()));
    }

    @Test
    public void secondItemIs20() {
        assertThat(test.get(1), samePropertyValuesAs(Question.builder()
                                                             .title("twenty")
                                                             .views(20)
                                                             .build()));
    }

    @Test
    public void thirdItemIs15() {
        assertThat(test.get(2), samePropertyValuesAs(Question.builder()
                                                             .title("fifteen")
                                                             .views(15)
                                                             .build()));
    }
}
