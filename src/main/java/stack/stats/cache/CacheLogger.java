package stack.stats.cache;

import org.ehcache.event.CacheEvent;
import org.ehcache.event.CacheEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CacheLogger implements CacheEventListener {
    Logger log = LoggerFactory.getLogger("stack.stats.cache.CacheLogger");

    @Override
    public void onEvent(CacheEvent event) {
        log.info("Cache item with key {} {}. ", event.getKey(), event.getType());
    }
}
