package stack.stats.cache;

import org.ehcache.expiry.ExpiryPolicy;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.function.Supplier;

public class CacheExpiryPolicy implements ExpiryPolicy {
    @Override
    public Duration getExpiryForCreation(Object key, Object value) {
        return Duration.between(LocalDateTime.now(), LocalDate.now().atTime(LocalTime.MAX));
    }

    @Override
    public Duration getExpiryForAccess(Object key, Supplier value) {
        return null;
    }

    @Override
    public Duration getExpiryForUpdate(Object key, Supplier oldValue, Object newValue) {
        return null;
    }
}
