package stack.stats.service;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import stack.stats.model.input.Question;
import stack.stats.model.input.StackExchangeWrapper;
import stack.stats.model.output.Indicator;
import stack.stats.model.output.Response;
import stack.stats.model.output.Series;
import stack.stats.util.DateConverter;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.ToIntFunction;
import java.util.stream.Collectors;

@Service
public class StatisticsService {
    private RestTemplate restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory());

    public <K> Map<K, Map<String, Long>> countByClassifiers(List<Question> questions, Function<Question, K> outerClassifier, Function<Question, String> innerClassifier) {
        return questions.stream()
                        .collect(Collectors.groupingBy(outerClassifier,
                                                       Collectors.groupingBy(innerClassifier, Collectors.counting())));
    }

    @Cacheable(value = "stats")
    public Response createResponse(String tag) {
        List<Question> questions = fetchQuestions(tag, 1);
        List<Question> mostViewed = findTopThree(questions, Question::getViews);
        List<Question> mostAnswered = findTopThree(questions, Question::getAnswers);
        List<Indicator> count = toChartJsonFormat(
                countByClassifiers(questions, DateConverter::findHourOfDay, DateConverter::findDayName));
        List<Indicator> answeredOrNot = toChartJsonFormat(
                countByClassifiers(questions, DateConverter::findDayName, Question::hasAnswers));
        return Response.builder()
                       .mostViewed(mostViewed)
                       .mostAnswered(mostAnswered)
                       .answeredOrNot(answeredOrNot)
                       .count(count)
                       .build();
    }

    public List<Question> fetchQuestions(String tag, int page) {
        long from = DateConverter.findLastOccurrenceOfDay(DateConverter.findBeginningOfToday());
        long to = DateConverter.findLastOccurrenceOfDay(DateConverter.findEndOfTomorrow());
        String url = "https://api.stackexchange.com/2.2/search/advanced?page={page}&pagesize=100&fromdate={from}&todate={to}&tagged={tag}&site=stackoverflow";
        StackExchangeWrapper stackExchangeWrapper = restTemplate.getForObject(url, StackExchangeWrapper.class, page,
                                                                              from, to, tag);
        List<Question> questions = stackExchangeWrapper.getItems();
        if (stackExchangeWrapper.isMore()) {
            questions.addAll(fetchQuestions(tag, ++page));
        }
        return questions;
    }


    public List<Question> findTopThree(List<Question> questions, ToIntFunction<Question> sortingByMethod) {
        return questions.stream()
                        .sorted(Comparator.comparingInt(sortingByMethod)
                                          .reversed())
                        .limit(3)
                        .collect(Collectors.toList());
    }

    public <K> List<Indicator> toChartJsonFormat(Map<K, Map<String, Long>> map) {
        return map.entrySet()
                  .stream()
                  .map(o -> new Indicator<>(o.getKey(), o.getValue()
                                                         .entrySet()
                                                         .stream()
                                                         .map(i -> new Series(i.getKey(), i.getValue()))
                                                         .collect(Collectors.toList())))
                  .collect(Collectors.toList());
    }
}
