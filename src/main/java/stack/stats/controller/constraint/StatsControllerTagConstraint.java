package stack.stats.controller.constraint;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = StatsControllerConstraintValidator.class)
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface StatsControllerTagConstraint {
    String message() default "Invalid tag";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
