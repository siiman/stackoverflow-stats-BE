package stack.stats.controller.constraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.stream.Stream;

public class StatsControllerConstraintValidator implements ConstraintValidator<StatsControllerTagConstraint, String> {
    @Override
    public void initialize(StatsControllerTagConstraint tagConstraint) {
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return Stream.of(ValidTags.values())
                     .anyMatch(t -> t.label.equalsIgnoreCase(value));
    }
}
