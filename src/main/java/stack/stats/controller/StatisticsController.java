package stack.stats.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import stack.stats.controller.constraint.StatsControllerTagConstraint;
import stack.stats.controller.constraint.ValidTags;
import stack.stats.model.output.Response;
import stack.stats.service.StatisticsService;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@Validated
@CrossOrigin(origins = "http://localhost:4200")
public class StatisticsController {
    @Autowired
    private StatisticsService statisticsService;

    @RequestMapping(value = "/stats", method = RequestMethod.GET)
    public Response response(@RequestParam(value = "tag") @StatsControllerTagConstraint String tag) {
        return statisticsService.createResponse(tag.toLowerCase());
    }

    @RequestMapping(value = "/tags", method = RequestMethod.GET)
    public List<String> tags() {
        return Stream.of(ValidTags.values())
                     .map(e -> e.label)
                     .sorted()
                     .collect(Collectors.toList());
    }
}
