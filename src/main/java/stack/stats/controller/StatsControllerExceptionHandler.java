package stack.stats.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpStatusCodeException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import java.io.IOException;

@ControllerAdvice
public class StatsControllerExceptionHandler {
    @ExceptionHandler(ConstraintViolationException.class)
    public void handleContraintExceptions(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value(), "Invalid tag");
    }

    @ExceptionHandler(HttpStatusCodeException.class)
    public void handleStackApiExceptions(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.BAD_GATEWAY.value(), "Error communicating with stackexchange API");

    }
}
