package stack.stats.model.input;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Question {
    @JsonProperty("view_count")
    private int views;
    @JsonProperty("answer_count")
    private int answers;
    @JsonProperty("creation_date")
    private long created;
    private String title;

    public String hasAnswers() {
        return this.answers < 1 ? "not answered" : "answered";
    }
}
