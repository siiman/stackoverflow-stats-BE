package stack.stats.model.output;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import stack.stats.model.input.Question;

import java.util.List;

@Builder
@Getter
@Setter
public class Response {
    @JsonProperty("most_viewed")
    private List<Question> mostViewed;
    @JsonProperty("most_answered")
    private List<Question> mostAnswered;
    private List<Indicator> count;
    @JsonProperty("answered_or_not")
    private List<Indicator> answeredOrNot;
}
