package stack.stats.model.output;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@Getter
@Setter
public class Indicator<T> {
    private T name;
    private List<Series> series;
}
