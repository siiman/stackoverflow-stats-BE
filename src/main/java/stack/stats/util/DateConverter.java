package stack.stats.util;

import stack.stats.model.input.Question;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.format.TextStyle;
import java.time.temporal.TemporalAdjusters;
import java.util.Locale;

public class DateConverter {
    public static String findDayName(Question question) {
        return LocalDateTime.ofEpochSecond(question.getCreated(), 0, ZoneOffset.UTC)
                            .getDayOfWeek()
                            .getDisplayName(TextStyle.FULL, Locale.getDefault());
    }

    public static int findHourOfDay(Question question) {
        return LocalDateTime.ofEpochSecond(question.getCreated(), 0, ZoneOffset.UTC)
                            .getHour();
    }

    public static long findLastOccurrenceOfDay(LocalDateTime dateTime) {
        return dateTime.with(TemporalAdjusters.previous(dateTime.getDayOfWeek()))
                       .toEpochSecond(ZoneOffset.UTC);
    }

    public static LocalDateTime findBeginningOfToday() {
        return LocalDate.now()
                        .atStartOfDay();
    }

    public static LocalDateTime findEndOfTomorrow() {
        return LocalDate.now()
                        .plusDays(1)
                        .atTime(LocalTime.MAX);
    }
}