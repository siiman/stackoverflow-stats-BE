# stackoverflow-stats-BE

A RESTful web service built with Spring Boot. 

It has two endpoints `/tags` and `/stats`. The former accepts GET requests and returns a list of acceptable tags for the `/stats` endpoint. The list consists of 100 most popular tags on Stackoverflow. 

`/stats` endpoint accepts GET requests with an input of a tag name. The endpoint has a constraint named `StatsControllerTagConstraint`. This is used to validate if the input is in the acceptable list of tags. 

`/stats` calls the method `StatisticsService.createResponse()`. This is sort of a wrapper method, that first fetches the data from Stack Exchange's public API for the given tag and dates (occurances of the day names of today and tomorrow last week), and then finds the specified statistical indicators. These include finding the top three most answered and most viewed questions, question distribution per hour, and how many questions have at least one answer and how many don't. 

The results of `StatisticsService.createResponse()` are cached using Ehcache 3 upon first request and expired on midnight. 

Both endpoints are used in the front end side of this project: https://github.com/siiman/stackoverflow-stats-FE

Clone this and run the `App.java` class to start the service. 
